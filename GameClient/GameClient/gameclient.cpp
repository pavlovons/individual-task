#include "gameclient.h"
#include <windows.h>

using namespace std;

GameClient::GameClient(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags), m_eGameState(ST_MAIN_MENU)
{
	InitializeWindow();
	InitializeMainMenu();
	socket = new QTcpSocket(this);
	connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
	connect(socket, SIGNAL(connected()), this, SLOT(connected()));
	m_CurrentPlayer = ENEMY;
}

GameClient::~GameClient()
{

}

void GameClient::InitializeWindow()
{
	this->setWindowTitle("Sea Battle");
}

void GameClient::InitializeMainMenu()
{
	m_eGameState = ST_MAIN_MENU;
	SetBackGroundMainMenu();
	SetFixedSizeWindow();
	InitializeButtons();
}

void GameClient::InitializeButtons()
{
	QRect ButtonCoords;
	ButtonCoords.setCoords(START_BUTTON_X1, START_BUTTON_Y1, START_BUTTON_X2, START_BUTTON_Y2);
	m_Buttons.SetCoordsStartButton(ButtonCoords);
	ButtonCoords.setCoords(LOCAL_GAME_BUTTON_X1, LOCAL_GAME_BUTTON_Y1, LOCAL_GAME_BUTTON_X2, LOCAL_GAME_BUTTON_Y2);
	m_Buttons.SetCoordsLocalGameButton(ButtonCoords);
	ButtonCoords.setCoords(SETTINGS_BUTTON_X1, SETTINGS_BUTTON_Y1, SETTINGS_BUTTON_X2, SETTINGS_BUTTON_Y2);
	m_Buttons.SetCoordsSettingsButton(ButtonCoords);
	ButtonCoords.setCoords(EXIT_BUTTON_X1, EXIT_BUTTON_Y1, EXIT_BUTTON_X2, EXIT_BUTTON_Y2);
	m_Buttons.SetCoordsExitButton(ButtonCoords);
	ButtonCoords.setCoords(CONNECT_BUTTON_X1, CONNECT_BUTTON_Y1, CONNECT_BUTTON_X2, CONNECT_BUTTON_Y2);
	m_Buttons.SetCoordsConnectButton(ButtonCoords);
	ButtonCoords.setCoords(BACK_BUTTON_X1, BACK_BUTTON_Y1, BACK_BUTTON_X2, BACK_BUTTON_Y2);
	m_Buttons.SetCoordsBackButton(ButtonCoords);
}

void GameClient::InitializeSimpleGame()
{
	m_eGameType = GT_WITH_PC;
	InitializePlacingShips();
}

void GameClient::InitializeLocalGame()
{
	m_eGameState = ST_LOCAL_MENU;
	m_eGameType = GT_LOCAL;
	SetBackGroundLocalMenu();	
}

void GameClient::InitializePlacingShips()
{
	m_eGameState = ST_PLACING_SHIPS;
	this->setMouseTracking(true);
	SetBackGroundGameField();
	m_GameField.ResetGameField();
}

void GameClient::InitializeStartGame()
{
	m_eGameState = ST_WAITING_STEP;
	this->setMouseTracking(false);
}

void GameClient::SetBackGroundMainMenu()
{
	QPalette pal;
	pal.setBrush(this->backgroundRole(), QBrush(QPixmap(MAIN_MENU_IMAGE_FILE)));
	this->setPalette(pal);
	this->setAutoFillBackground(true);
	repaint();
}

void GameClient::SetBackGroundLocalMenu()
{
	QPalette pal;
	pal.setBrush(this->backgroundRole(), QBrush(QPixmap(LOCAL_MENU_IMAGE_FILE)));
	this->setPalette(pal);
	this->setAutoFillBackground(true);
	repaint();
}

void GameClient::SetFixedSizeWindow()
{
	this->setFixedSize(SCREEN_WEIGHT, SCREEN_HEIGHT);
	repaint();
}

void GameClient::SetBackGroundGameField()
{
	QPalette pal;
	pal.setBrush(this->backgroundRole(), QBrush(QPixmap(GAME_FIELD_IMAGE_FILE)));
	this->setPalette(pal);
	this->setAutoFillBackground(true);
	repaint();
}

void GameClient::paintEvent( QPaintEvent *event )
{
	if ( m_eGameState != ST_LOCAL_MENU && m_eGameState != ST_MAIN_MENU )
	{
		QPainter painter(this);
		QBrush brush(Qt::NoBrush);
		QPen pen = QPen();
		pen.setWidth(2);
		painter.setPen(pen);
		QString MyFieldString = m_GameField.MyFieldToStr();
		QString EnemyFieldString = m_GameField.EnemyFieldToStr();
		int iMyFieldCellLengthX = (MY_FIELD_X2 - MY_FIELD_X1) / DEFAULT_FIELD_SIZE;
		int iMyFieldCellLengthY = (MY_FIELD_Y2 - MY_FIELD_Y1) / DEFAULT_FIELD_SIZE;
		painter.setBrush(brush);
		for( int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
		{
			for( int j = 0; j < DEFAULT_FIELD_SIZE; ++j )
			{
				QPoint FigureCoord = TransformGameFieldCoordsToMyFieldCoords(QPoint(i,j));
				if ( MyFieldString[j * DEFAULT_FIELD_SIZE + i] == X_CELL)
				{		
					painter.drawLine(FigureCoord, QPoint(FigureCoord.x() + iMyFieldCellLengthX, FigureCoord.y() + iMyFieldCellLengthY));
					painter.drawLine(FigureCoord.x() + iMyFieldCellLengthX, FigureCoord.y(), FigureCoord.x(), FigureCoord.y() + iMyFieldCellLengthY);
				}
				else if ( MyFieldString[j * DEFAULT_FIELD_SIZE + i] == SHIP_CELL)
				{
					painter.drawRect( FigureCoord.x(), FigureCoord.y(), iMyFieldCellLengthX, iMyFieldCellLengthY);
				}
				else if ( MyFieldString[j * DEFAULT_FIELD_SIZE + i] == DOT_CELL)
				{
					brush.setStyle(Qt::SolidPattern);
					FigureCoord.setX(FigureCoord.x() + iMyFieldCellLengthX / 2);
					FigureCoord.setY(FigureCoord.y() + iMyFieldCellLengthY / 2);
					painter.drawEllipse(FigureCoord, iMyFieldCellLengthX / 6 , iMyFieldCellLengthY / 6);
				}				
			}
		}	
		int iEnemyFieldCellLengthX = (ENEMY_FIELD_X2 - ENEMY_FIELD_X1) / DEFAULT_FIELD_SIZE;
		int iEnemyFieldCellLengthY = (ENEMY_FIELD_Y2 - ENEMY_FIELD_Y1) / DEFAULT_FIELD_SIZE;
		for( int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
		{
			for( int j = 0; j < DEFAULT_FIELD_SIZE; ++j )
			{
				QPoint FigureCoord = TransformGameFieldCoordsToEnemyFieldCoords(QPoint(i,j));
				if ( EnemyFieldString[j * DEFAULT_FIELD_SIZE + i] == X_CELL)
				{
					painter.drawLine(FigureCoord, QPoint(FigureCoord.x() + iEnemyFieldCellLengthX, FigureCoord.y() + iEnemyFieldCellLengthY));
					painter.drawLine(FigureCoord.x() + iEnemyFieldCellLengthX, FigureCoord.y(), FigureCoord.x(), FigureCoord.y() + iEnemyFieldCellLengthY);
				}
				else if ( EnemyFieldString[j * DEFAULT_FIELD_SIZE + i] == SHIP_CELL)
				{
					painter.drawRect( FigureCoord.x(), FigureCoord.y(), iEnemyFieldCellLengthX, iEnemyFieldCellLengthY );
				}
				else if ( EnemyFieldString[j * DEFAULT_FIELD_SIZE + i] == DOT_CELL)
				{					
					FigureCoord.setX(FigureCoord.x() + iEnemyFieldCellLengthX / 2);
					FigureCoord.setY(FigureCoord.y() + iEnemyFieldCellLengthY / 2);
					painter.drawEllipse(FigureCoord, iEnemyFieldCellLengthX / 6 , iEnemyFieldCellLengthY / 6);
				}				
			}
		}
	}
}

void GameClient::mousePressEvent( QMouseEvent *ev )
{
	QPoint ClickPoint = ev->pos();
	//char str[50];
	//sprintf(str,"%d %d",ClickPoint.x(),ClickPoint.y());
	//QMessageBox::information(0, "Position", str);
	if ( m_eGameState == ST_MAIN_MENU )
	{
		if ( ev->button() == Qt::LeftButton )
		{
			if ( m_Buttons.IsExitButton( ClickPoint ) )
			{
				CheckForExit();
			}
			if ( m_Buttons.IsSettingsButton( ClickPoint ) )
			{
				QMessageBox::information(0, "Position", "Settings");
			}
			if ( m_Buttons.IsStartButton( ClickPoint ) )
			{
				InitializeSimpleGame();
			}
			if ( m_Buttons.IsLocalGameButton( ClickPoint ) )
			{
				InitializeLocalGame();
			}
		}
	}
	else if ( m_eGameState == ST_PLACING_SHIPS )
	{
		if ( ev->button() == Qt::RightButton )
		{
			m_GameField.ChangeShipDirection();
		}
		else if ( ev->button() == Qt::LeftButton )
		{
			if( InMyGameField( ClickPoint ) )
			{
				PlaceShip( ClickPoint );
			}
			if( m_Buttons.IsBackButton( ClickPoint ) )
			{
				if ( socket != nullptr)
				{
					socket->disconnect();
					socket->~QTcpSocket();
					socket = nullptr;
				}
				InitializeMainMenu();
			}
		}
	}
	else if ( m_eGameState == ST_LOCAL_MENU )
	{
		if ( ev->button() == Qt::LeftButton )
		{
			if ( m_Buttons.IsConnectButton( ClickPoint ) )
			{
				socket->connectToHost("127.0.0.1", 4200);
				if ( socket == nullptr ) 
				{
					QMessageBox::information(0, "Server", "Unable to connect to server");
				}
			}
			if( m_Buttons.IsBackButton( ClickPoint ) )
			{				
				if ( socket != nullptr)
				{
					socket->disconnect();
					socket->~QTcpSocket();
					socket = nullptr;
				}
				InitializeMainMenu();
			}
		}
	}
	else if ( m_eGameState == ST_MAKING_STEP )
	{
		if ( m_CurrentPlayer == MY)
		{
			if ( InEnemyGameField( ClickPoint ) )
			{
				QPoint FieldPoint = TransformEnemyFieldCoordsToGameFieldCoords( ClickPoint );
				if ( m_GameField.IsNotShooted( FieldPoint ) )
				{
					char buffer[BUFFER_SIZE];
					sprintf( buffer,
						"<seabattle command=\"step\">"
						"	<element x=\"%d\"/>"
						"	<element y=\"%d\"/>"
						"</seabattle>", FieldPoint.y(), FieldPoint.x());
					socket->write(QString(buffer).toUtf8());				
					m_eGameState = ST_WAITING_STEP;
				}
			}
		}
	}
	repaint();
}

void GameClient::mouseMoveEvent( QMouseEvent *ev )
{
	if ( m_eGameState == ST_PLACING_SHIPS )
	{
		QPoint mousePosition = ev->pos();
	}
}

void GameClient::closeEvent( QCloseEvent *event )
{
	event->setAccepted(false);
	CheckForExit();
}

void GameClient::CheckForExit() const
{
	QMessageBox msgBox;
	msgBox.setWindowTitle("Sea Battle");
	msgBox.setText("                       Exit                       ");
	msgBox.setIcon(QMessageBox::Information);
	msgBox.setInformativeText("Do you really want to leave?");
	msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
	msgBox.setDefaultButton(QMessageBox::Cancel);
	int ret = msgBox.exec();
	switch (ret) 
	{
	case QMessageBox::Ok:
		QApplication::quit();
		break;
	default:

		break;
	}
}

void GameClient::PlaceShip( QPoint &ClickPoint )
{
	QPoint BeginShipPoint = TransformMyFieldCoordsToGameFieldCoords( ClickPoint );
	QPoint EndShipPoint;
	if ( m_GameField.IsItPossibleToPutShip( BeginShipPoint, &EndShipPoint ) )
	{
		m_GameField.PlaceShip( BeginShipPoint, EndShipPoint );		
	}
	else
	{
		QMessageBox::information(0, "Position", "Wrong ship placement");
	}
	if ( !m_GameField.IsShipsRemainings() )
	{
		QMessageBox::information(0, "Sea Battle", "Wait for opponent");
		QString FieldString = m_GameField.MyFieldToStr();
		char *xmlText = new char[BUFFER_SIZE];
		sprintf( xmlText,
			"<seabattle command=\"field\">"
			"	<element cells=\"%s\"/>"		
			"</seabattle>", FieldString.toStdString().c_str() );
		socket->write(QString(xmlText).toUtf8());
		InitializeStartGame();
	}
}

void GameClient::readyRead()
{
	QString line = QString::fromUtf8(socket->readLine()).trimmed();
	QDomDocument doc;
	doc.setContent(line);
	QDomElement rootElement= doc.documentElement();
	QString rootName = rootElement.tagName();
	if ( rootName == "seabattle" )
	{	
		if ( rootElement.attribute("command","") == "server" )
		{
			QDomNode domNode = rootElement.firstChild();
			QString login;
			if ( domNode.isElement() && !domNode.isNull())
			{
				QDomElement domElement = domNode.toElement();
				if(!domElement.isNull())
				{
					if(domElement.tagName() == "element") 
					{
						login = domElement.attribute("name", "");
					}
				}
			}
			QMessageBox::information(0, "Server", "Connected");
			InitializePlacingShips();
		}
		else if ( rootElement.attribute("command","") == "go" )
		{
			m_eGameState = ST_MAKING_STEP;
			m_CurrentPlayer = MY;
			QMessageBox::information(0, "Server", "You turn!");
		}
		else if ( rootElement.attribute("command","") == "miss" )
		{
			QDomNode domNode = rootElement.firstChild();
			int x, y;
			if ( domNode.isElement() && !domNode.isNull())
			{
				QDomElement domElement = domNode.toElement();
				if(!domElement.isNull())
				{
					if(domElement.tagName() == "element") 
					{
						x = domElement.attribute("x", "").toInt();							
					}
				}
			}
			domNode = domNode.nextSibling();
			if ( domNode.isElement() && !domNode.isNull())
			{
				QDomElement domElement = domNode.toElement();
				if(!domElement.isNull())
				{
					if(domElement.tagName() == "element") 
					{
						y = domElement.attribute("y", "").toInt();		
					}
				}
			}
			if ( m_CurrentPlayer == ENEMY )
			{
				m_CurrentPlayer = MY;
				m_GameField.SetMyCellDot( QPoint(x,y) );
				m_eGameState = ST_MAKING_STEP;
				QMessageBox::information(0, "Server", "You turn!");
			}
			else
			{
				m_CurrentPlayer = ENEMY;
				m_GameField.SetEnemyCellDot( QPoint(x,y) );
				m_eGameState = ST_WAITING_STEP;
			}
		}
		else if ( rootElement.attribute("command","") == "half" )
		{
			QDomNode domNode = rootElement.firstChild();
			int x, y;
			if ( domNode.isElement() && !domNode.isNull())
			{
				QDomElement domElement = domNode.toElement();
				if(!domElement.isNull())
				{
					if(domElement.tagName() == "element") 
					{
						x = domElement.attribute("x", "").toInt();							
					}
				}
			}
			domNode = domNode.nextSibling();
			if ( domNode.isElement() && !domNode.isNull())
			{
				QDomElement domElement = domNode.toElement();
				if(!domElement.isNull())
				{
					if(domElement.tagName() == "element") 
					{
						y = domElement.attribute("y", "").toInt();		
					}
				}
			}
			if ( m_CurrentPlayer == ENEMY )
			{
				m_GameField.SetMyCellX( QPoint(x,y) );
			}
			else
			{
				m_GameField.SetEnemyCellX( QPoint(x,y) );
				m_eGameState = ST_MAKING_STEP;
			}
		}
		else if ( rootElement.attribute("command","") == "kill" )
		{
			QDomNode domNode = rootElement.firstChild();
			int x, y;
			if ( domNode.isElement() && !domNode.isNull())
			{
				QDomElement domElement = domNode.toElement();
				if(!domElement.isNull())
				{
					if(domElement.tagName() == "element") 
					{
						x = domElement.attribute("x", "").toInt();							
					}
				}
			}
			domNode = domNode.nextSibling();
			if ( domNode.isElement() && !domNode.isNull())
			{
				QDomElement domElement = domNode.toElement();
				if(!domElement.isNull())
				{
					if(domElement.tagName() == "element") 
					{
						y = domElement.attribute("y", "").toInt();		
					}
				}
			}
			if ( m_CurrentPlayer == ENEMY )
			{
				m_GameField.SetMyCellX( QPoint(x,y) );
			}
			else
			{
				m_GameField.SetEnemyCellX( QPoint(x,y) );
				m_eGameState = ST_MAKING_STEP;
				QMessageBox::information(0, "Sea Battle", "You killed ship.");
			}
		}
		else if ( rootElement.attribute("command","") == "win" )
		{
			socket->disconnect();
			QMessageBox::information(0, "Sea Battle", "Congratulations! You win.");
			InitializeMainMenu();
		}
		else if ( rootElement.attribute("command","") == "lose" )
		{
			socket->disconnect();
			socket->~QTcpSocket();
			socket = nullptr;
			QMessageBox::information(0, "Sea Battle", "Congratulations! You lose.");
			InitializeMainMenu();
		}
		else
		{
			//bad command
		}
	}
	else
	{
		
	}
	repaint();
}

void GameClient::connected()
{
	QByteArray xmlText =
		"<seabattle command=\"client\">"
		"	<element login=\"rhobar\"/>"		
		"</seabattle>";
	socket->write(QString(xmlText).toUtf8());
}
