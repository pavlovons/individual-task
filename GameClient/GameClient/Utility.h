#ifndef _UTILITY_H_
#define _UTILITY_H_

#include <QtGui/QMainWindow>

#include "Constants.h"

bool InMyGameField( QPoint& );

bool InEnemyGameField( QPoint& );

QPoint TransformMyFieldCoordsToGameFieldCoords( QPoint& );

QPoint TransformEnemyFieldCoordsToGameFieldCoords( QPoint& );

QPoint TransformGameFieldCoordsToMyFieldCoords( QPoint& );

QPoint TransformGameFieldCoordsToEnemyFieldCoords( QPoint& );

#endif /* _UTILITY_H_ */
