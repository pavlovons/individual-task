#include "Ships.h"

CShips::CShips() : m_iShipsCount(LIMIT_FOUR_DIMENTION_SHIPS), m_eShipDirection(SD_VERTICAL), m_eCurrentShipType(ST_FOUR)
{
	
}

CShips::~CShips()
{

}

void CShips::ResetShips()
{
	m_iShipsCount = LIMIT_FOUR_DIMENTION_SHIPS;
	m_eShipDirection = SD_VERTICAL;
	m_eCurrentShipType = ST_FOUR;	
}

void CShips::NextShip()
{
	if ( m_iShipsCount > 0 )
	{
		--m_iShipsCount;
		m_eShipDirection = SD_VERTICAL;
		if ( m_iShipsCount == LIMIT_THREE_DIMENTION_SHIPS )
		{
			m_eCurrentShipType = ST_THREE;
		}
		else if ( m_iShipsCount == LIMIT_TWO_DIMENTION_SHIPS )
		{
			m_eCurrentShipType = ST_TWO;
		}
		else if ( m_iShipsCount == LIMIT_ONE_DIMENTION_SHIPS )
		{
			m_eCurrentShipType = ST_ONE;
		}
	} 
}

void CShips::ChangeShipDirection()
{
	if ( m_eShipDirection == SD_HORIZONTAL )
	{
		m_eShipDirection = SD_VERTICAL;
	} 
	else
	{
		m_eShipDirection = SD_HORIZONTAL;
	}
}

int CShips::GetShipsCount() const
{
	return m_iShipsCount;
}

CShips::TShipDirection CShips::GetShipDirection() const
{
	return m_eShipDirection;
}

CShips::TShipType CShips::GetCurrentShipType() const
{
	return m_eCurrentShipType;
}