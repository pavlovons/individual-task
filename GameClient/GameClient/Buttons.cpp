#include "Buttons.h"

CButtons::CButtons()
{

}

CButtons::~CButtons()
{

}

bool CButtons::IsStartButton( QPoint &inputPoint ) const
{
	int x1,x2,y1,y2;
	m_RectStartButton.getCoords(&x1, &y1, &x2, &y2);
	return ( inputPoint.x() >= x1  && inputPoint.x() <= x2 && inputPoint.y() >= y1 && inputPoint.y() <= y2);
}

bool CButtons::IsSettingsButton( QPoint& inputPoint ) const
{
	int x1,x2,y1,y2;
	m_RectSettingsButton.getCoords(&x1, &y1, &x2, &y2);
	return ( inputPoint.x() >= x1  && inputPoint.x() <= x2 && inputPoint.y() >= y1 && inputPoint.y() <= y2);
}

bool CButtons::IsLocalGameButton( QPoint& inputPoint ) const
{
	int x1,x2,y1,y2;
	m_RectLocalGameButton.getCoords(&x1, &y1, &x2, &y2);
	return ( inputPoint.x() >= x1  && inputPoint.x() <= x2 && inputPoint.y() >= y1 && inputPoint.y() <= y2);
}

bool CButtons::IsExitButton( QPoint& inputPoint ) const
{
	int x1,x2,y1,y2;
	m_RectExitButton.getCoords(&x1, &y1, &x2, &y2);
	return ( inputPoint.x() >= x1  && inputPoint.x() <= x2 && inputPoint.y() >= y1 && inputPoint.y() <= y2);
}

bool CButtons::IsConnectButton( QPoint& inputPoint ) const
{
	int x1,x2,y1,y2;
	m_RectConnectButton.getCoords(&x1, &y1, &x2, &y2);
	return ( inputPoint.x() >= x1  && inputPoint.x() <= x2 && inputPoint.y() >= y1 && inputPoint.y() <= y2);
}

bool CButtons::IsBackButton( QPoint& inputPoint ) const
{
	int x1,x2,y1,y2;
	m_RectBackButton.getCoords(&x1, &y1, &x2, &y2);
	return ( inputPoint.x() >= x1  && inputPoint.x() <= x2 && inputPoint.y() >= y1 && inputPoint.y() <= y2);
}

const QRect CButtons::GetCoordsStartButton() const
{
	return m_RectStartButton;
}

const QRect CButtons::GetCoordsLocalGameButton() const
{
	return m_RectLocalGameButton;
}

const QRect CButtons::GetCoordsSettingsButton() const
{
	return m_RectSettingsButton;
}

const QRect CButtons::GetCoordsExitButton() const
{
	return m_RectExitButton;
}

const QRect CButtons::GetCoordsConnectButton() const
{
	return m_RectConnectButton;
}

const QRect CButtons::GetCoordsBackButton() const
{
	return m_RectBackButton;
}

void CButtons::SetCoordsStartButton( QRect &rectCoords )
{
	m_RectStartButton = QRect(rectCoords);
}

void CButtons::SetCoordsLocalGameButton( QRect &rectCoords )
{
	m_RectLocalGameButton = QRect(rectCoords);
}

void CButtons::SetCoordsSettingsButton( QRect &rectCoords )
{
	m_RectSettingsButton = QRect(rectCoords);
}

void CButtons::SetCoordsExitButton( QRect &rectCoords )
{
	m_RectExitButton = QRect(rectCoords);
}

void CButtons::SetCoordsConnectButton( QRect &rectCoords )
{
	m_RectConnectButton = QRect(rectCoords);
}


void CButtons::SetCoordsBackButton( QRect &rectCoords )
{
	m_RectBackButton = QRect(rectCoords);
}