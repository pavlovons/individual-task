#include "GameField.h"

CGameField::CGameField()
{
	for ( int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
	{
		for ( int j = 0; j < DEFAULT_FIELD_SIZE; ++j )
		{
			m_cMyField[i * DEFAULT_FIELD_SIZE + j] = EMPTY_CELL;
		}
	}
	for ( int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
	{
		for ( int j = 0; j < DEFAULT_FIELD_SIZE; ++j )
		{
			m_cEnemyField[i * DEFAULT_FIELD_SIZE + j] = EMPTY_CELL;
		}
	}
	m_Ships.ResetShips();
}

CGameField::~CGameField()
{

}


bool CGameField::IsNotShooted( QPoint& currentCell ) const
{
	return (m_cEnemyField[currentCell.y() * DEFAULT_FIELD_SIZE + currentCell.x()] == EMPTY_CELL);
}

void CGameField::SetEnemyCellX( QPoint& currentCell )
{
	m_cEnemyField[currentCell.x() * DEFAULT_FIELD_SIZE + currentCell.y()] = X_CELL;
}

void CGameField::SetEnemyCellDot( QPoint& currentCell )
{
	m_cEnemyField[currentCell.x() * DEFAULT_FIELD_SIZE + currentCell.y()] = DOT_CELL;
}

void CGameField::SetMyCellX( QPoint& currentCell )
{
	m_cMyField[currentCell.x() * DEFAULT_FIELD_SIZE + currentCell.y()] = X_CELL;
}

void CGameField::SetMyCellDot( QPoint& currentCell )
{
	m_cMyField[currentCell.x() * DEFAULT_FIELD_SIZE + currentCell.y()] = DOT_CELL;
}
 
bool CGameField::IsShipsRemainings() const
{
	return m_Ships.GetShipsCount();
}

void CGameField::ResetGameField()
{
	for ( int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
	{
		for ( int j = 0; j < DEFAULT_FIELD_SIZE; ++j )
		{
			m_cMyField[i * DEFAULT_FIELD_SIZE + j] = EMPTY_CELL;
		}
	}
	for ( int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
	{
		for ( int j = 0; j < DEFAULT_FIELD_SIZE; ++j )
		{
			m_cEnemyField[i * DEFAULT_FIELD_SIZE + j] = EMPTY_CELL;
		}
	}
	m_Ships.ResetShips();
}

void CGameField::ChangeShipDirection()
{
	m_Ships.ChangeShipDirection();
}

 CShips::TShipDirection CGameField::GetShipDirection() const
 {
	 return m_Ships.GetShipDirection();
 }
 
 int CGameField::GetCurrentShipType() const
 {
	 int iResult;
	 if ( m_Ships.GetCurrentShipType() == CShips::ST_ONE )
	 {
		 iResult = 1;
	 }
	 else if ( m_Ships.GetCurrentShipType() == CShips::ST_TWO )
	 {
		 iResult = 2;
	 }
	 else if ( m_Ships.GetCurrentShipType() == CShips::ST_THREE )
	 {
		 iResult = 3;
	 }
	 else
	 {
		 iResult = 4;
	 }
	 return iResult;
 }

bool CGameField::IsItPossibleToPutShip( QPoint &BeginShipCoords, QPoint *EndShipCoords ) const
{
	bool bResult = true;
	QPoint EndCoords;
	if ( GetEndShipCoords( BeginShipCoords, &EndCoords ) )
	{
		EndShipCoords->setX( EndCoords.x() );
		EndShipCoords->setY( EndCoords.y() );
		int x1 = BeginShipCoords.x();
		int x2 = EndCoords.x();
		int y1 = BeginShipCoords.y();
		int y2 = EndCoords.y();
		for( int i = x1 - 1; i <= x2 + 1; ++i)
		{
			if ( i >= 0 && i < DEFAULT_FIELD_SIZE )
			{
				for( int j = y1 - 1; j <= y2 + 1; ++j)
				{
					if ( j >= 0 && j < DEFAULT_FIELD_SIZE )
					{
						if ( m_cMyField[j * DEFAULT_FIELD_SIZE + i] == SHIP_CELL )
						{
							bResult = false;
							break;
						}
					}	
				}
			}
			if ( !bResult )
			{	
				break;
			}
		}
	}
	else
	{
		bResult = false;
	}
	return bResult;
}

void CGameField::PlaceShip( QPoint &BeginShipCoords, QPoint &EndShipCoords )
{
	int x1 = BeginShipCoords.x();
	int x2 = EndShipCoords.x();
	int y1 = BeginShipCoords.y();
	int y2 = EndShipCoords.y();
	if ( x1 == x2 )
	{
		for ( int i = y1; i <= y2; ++i )
		{
			m_cMyField[i * DEFAULT_FIELD_SIZE + x1] = SHIP_CELL;
		}
	}
	if ( y1 == y2 )
	{
		for ( int i = x1; i <= x2; ++i )
		{
			m_cMyField[y1 * DEFAULT_FIELD_SIZE + i] = SHIP_CELL;
		}
	}
	m_Ships.NextShip();
}

bool CGameField::GetEndShipCoords( QPoint &BeginCoords, QPoint *EndCoords ) const
{
	bool bResult = false;
	CShips::TShipType CurrentShipType = m_Ships.GetCurrentShipType();
	if ( m_Ships.GetShipDirection() == CShips::SD_VERTICAL )
	{
		EndCoords->setX(BeginCoords.x());
		switch( CurrentShipType )
		{
		case CShips::ST_FOUR:
			EndCoords->setY(BeginCoords.y() + 3);
			break;
		case CShips::ST_THREE:
			EndCoords->setY(BeginCoords.y() + 2);
			break;
		case CShips::ST_TWO:
			EndCoords->setY(BeginCoords.y() + 1);
			break;
		default:
			EndCoords->setY(BeginCoords.y());
			break;
		}
	}
	else
	{
		EndCoords->setY(BeginCoords.y());
		switch( CurrentShipType )
		{
		case CShips::ST_FOUR:
			EndCoords->setX(BeginCoords.x() + 3);
			break;
		case CShips::ST_THREE:
			EndCoords->setX(BeginCoords.x() + 2);
			break;
		case CShips::ST_TWO:
			EndCoords->setX(BeginCoords.x() + 1);
			break;
		default:
			EndCoords->setX(BeginCoords.x());
			break;
		}
	}
	if ( EndCoords->x() >= 0 && EndCoords->x() < DEFAULT_FIELD_SIZE && EndCoords->y() >= 0 && EndCoords->y() < DEFAULT_FIELD_SIZE )
	{
		bResult = true;
	}
	return bResult;
}

QString CGameField::MyFieldToStr()const
{
	QString str = QString(DEFAULT_FIELD_SIZE * DEFAULT_FIELD_SIZE, Qt::Uninitialized);
	for( int i = 0; i < DEFAULT_FIELD_SIZE * DEFAULT_FIELD_SIZE; ++i )
	{
		str[i] = m_cMyField[i];
	}
	return str;
}	

QString CGameField::EnemyFieldToStr() const
{
	QString str = QString(DEFAULT_FIELD_SIZE * DEFAULT_FIELD_SIZE, Qt::Uninitialized);
	for( int i = 0; i < DEFAULT_FIELD_SIZE * DEFAULT_FIELD_SIZE; ++i )
	{
		str[i] = m_cEnemyField[i];
	}
	return str;
}

