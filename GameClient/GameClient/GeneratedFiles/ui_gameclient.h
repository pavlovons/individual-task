/********************************************************************************
** Form generated from reading UI file 'gameclient.ui'
**
** Created: Tue 14. May 14:18:39 2013
**      by: Qt User Interface Compiler version 4.8.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAMECLIENT_H
#define UI_GAMECLIENT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GameClientClass
{
public:
    QWidget *centralWidget;

    void setupUi(QMainWindow *GameClientClass)
    {
        if (GameClientClass->objectName().isEmpty())
            GameClientClass->setObjectName(QString::fromUtf8("GameClientClass"));
        GameClientClass->resize(722, 544);
        GameClientClass->setDockOptions(QMainWindow::AllowTabbedDocks|QMainWindow::AnimatedDocks);
        centralWidget = new QWidget(GameClientClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setEnabled(true);
        GameClientClass->setCentralWidget(centralWidget);

        retranslateUi(GameClientClass);

        QMetaObject::connectSlotsByName(GameClientClass);
    } // setupUi

    void retranslateUi(QMainWindow *GameClientClass)
    {
        GameClientClass->setWindowTitle(QApplication::translate("GameClientClass", "GameClient", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class GameClientClass: public Ui_GameClientClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAMECLIENT_H
