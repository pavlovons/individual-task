#include "Utility.h"

bool InMyGameField( QPoint& CurrentPoint)
{
	bool bResult = false;
	if ( CurrentPoint.x() >= MY_FIELD_X1 && CurrentPoint.x() <= MY_FIELD_X2 && CurrentPoint.y() >= MY_FIELD_Y1 && CurrentPoint.y() <= MY_FIELD_Y2 )
	{
		bResult = true;
	}
	return bResult;
}

bool InEnemyGameField( QPoint& CurrentPoint)
{
	bool bResult = false;
	if ( CurrentPoint.x() >= ENEMY_FIELD_X1 && CurrentPoint.x() <= ENEMY_FIELD_X2 && CurrentPoint.y() >= ENEMY_FIELD_Y1 && CurrentPoint.y() <= ENEMY_FIELD_Y2 )
	{
		bResult = true;
	}
	return bResult;
}

QPoint TransformMyFieldCoordsToGameFieldCoords( QPoint& CurrentPoint )
{
	int iGameFieldLengthX = MY_FIELD_X2 - MY_FIELD_X1;
	int iGameFieldLengthY = MY_FIELD_Y2 - MY_FIELD_Y1;
	QPoint ResultPoint;
	for ( int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
	{
		if ( CurrentPoint.x() >= (MY_FIELD_X1 + i * iGameFieldLengthX / 10.0) && CurrentPoint.x() <= (MY_FIELD_X2 + (i + 1) * iGameFieldLengthX / 10.0) )
		{
			ResultPoint.setX(i);
		}
	}
	for ( int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
	{
		if ( CurrentPoint.y() >= (MY_FIELD_Y1 + i * iGameFieldLengthY / 10.0) && CurrentPoint.y() <= (MY_FIELD_Y2 + (i + 1) * iGameFieldLengthY / 10.0) )
		{
			ResultPoint.setY(i);
		}
	}
	return ResultPoint;
}

QPoint TransformEnemyFieldCoordsToGameFieldCoords( QPoint& CurrentPoint )
{
	int iGameFieldLengthX = ENEMY_FIELD_X2 - ENEMY_FIELD_X1;
	int iGameFieldLengthY = ENEMY_FIELD_Y2 - ENEMY_FIELD_Y1;
	QPoint ResultPoint;
	for ( int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
	{
		if ( CurrentPoint.x() >= (ENEMY_FIELD_X1 + i * iGameFieldLengthX / 10.0) && CurrentPoint.x() <= (ENEMY_FIELD_X2 + (i + 1) * iGameFieldLengthX / 10.0) )
		{
			ResultPoint.setX(i);
		}
	}
	for ( int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
	{
		if ( CurrentPoint.y() >= (ENEMY_FIELD_Y1 + i * iGameFieldLengthY / 10.0) && CurrentPoint.y() <= (ENEMY_FIELD_Y2 + (i + 1) * iGameFieldLengthY / 10.0) )
		{
			ResultPoint.setY(i);
		}
	}
	return ResultPoint;
}

QPoint TransformGameFieldCoordsToMyFieldCoords( QPoint& CurrentPoint )
{
	QPoint ResultPoint = QPoint(MY_FIELD_X1 + CurrentPoint.x() * (MY_FIELD_X2 - MY_FIELD_X1) / DEFAULT_FIELD_SIZE, 
									MY_FIELD_Y1 + CurrentPoint.y() * (MY_FIELD_Y2 - MY_FIELD_Y1) / DEFAULT_FIELD_SIZE); 
	return ResultPoint;
}

QPoint TransformGameFieldCoordsToEnemyFieldCoords( QPoint& CurrentPoint )
{
	QPoint ResultPoint = QPoint(ENEMY_FIELD_X1 + CurrentPoint.x() * (ENEMY_FIELD_X2 - ENEMY_FIELD_X1) / DEFAULT_FIELD_SIZE,
									ENEMY_FIELD_Y1 + CurrentPoint.y() * (ENEMY_FIELD_Y2 - ENEMY_FIELD_Y1) / DEFAULT_FIELD_SIZE); 
	return ResultPoint;
}