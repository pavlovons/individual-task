#ifndef _GAME_CLIENT_H_
#define _GAME_CLIENT_H_

#include <QtGui/QMainWindow>
#include <QtGui/QMouseEvent>
#include <QtGui/QMessageBox>
#include <QTcpSocket>
#include <QDomDocument>
#include <QPainter>
#include "ui_gameclient.h"

#include "Constants.h"
#include "Buttons.h"
#include "GameField.h"
#include "Utility.h"

class GameClient : public QMainWindow , public Ui::GameClientClass
{
	Q_OBJECT

public:

	enum State
	{
		ST_MAIN_MENU,
		ST_LOCAL_MENU,
		ST_PLACING_SHIPS,
		ST_WAITING_STEP,
		ST_MAKING_STEP
	};

	enum PLAYER
	{
		MY,
		ENEMY
	};

	enum GameType
	{
		GT_WITH_PC,
		GT_LOCAL
	};

	typedef State TState;
	typedef GameType TGameType;

	GameClient(QWidget *parent = 0, Qt::WFlags flags = 0);
	~GameClient();

	void InitializeWindow();
	void InitializeMainMenu();
	void InitializeButtons();
	void InitializeSimpleGame();
	void InitializeLocalGame();
	void InitializeStartGame();
	void InitializePlacingShips();

	void SetBackGroundMainMenu();
	void SetBackGroundGameField();
	void SetBackGroundLocalMenu();
	void SetFixedSizeWindow();	

	void paintEvent( QPaintEvent* );
	void mousePressEvent( QMouseEvent* );
	void mouseMoveEvent( QMouseEvent* );
	void closeEvent( QCloseEvent* );

	void CheckForExit() const;

	void PlaceShip( QPoint& );

private slots:
	void readyRead();
	void connected();

private:

	// This is the socket that will let us communitate with the server.
	QTcpSocket *socket;
	Ui::GameClientClass ui;
	TState m_eGameState;
	PLAYER m_CurrentPlayer;
	TGameType m_eGameType;
	CButtons m_Buttons;
	CGameField m_GameField;
};

#endif // _GAMECLIENT_H_
