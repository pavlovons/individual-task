#ifndef _GAME_FIELD_H_
#define _GAME_FIELD_H_

#define SHIP_CELL '1'
#define EMPTY_CELL '0'
#define X_CELL 'X'
#define DOT_CELL '.'

#include <QtGui/QMainWindow>

#include "Constants.h"
#include "Ships.h"

class CGameField
{
public:

	typedef char TField[DEFAULT_FIELD_SIZE * DEFAULT_FIELD_SIZE];
	
	CGameField();
	~CGameField();

	void ResetGameField();

	void ChangeShipDirection();

	CShips::TShipDirection GetShipDirection() const;

	int GetCurrentShipType() const;

	// Second argument returns EndShipCoords
	bool IsItPossibleToPutShip( QPoint&, QPoint* ) const;

	bool GetEndShipCoords( QPoint&, QPoint* ) const;

	bool IsShipsRemainings() const;
	
	void PlaceShip( QPoint&, QPoint& );

	bool IsNotShooted( QPoint& ) const;

	void SetEnemyCellX( QPoint& );

	void SetEnemyCellDot( QPoint& );

	void SetMyCellX( QPoint& );

	void SetMyCellDot( QPoint& );

	QString MyFieldToStr() const;
	QString EnemyFieldToStr() const;

private:
	TField m_cMyField, m_cEnemyField;
	CShips m_Ships;
};

#endif /* _GAME_FIELD_H_ */