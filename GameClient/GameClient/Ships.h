#ifndef _SHIPS_H_
#define _SHIPS_H_

#define LIMIT_FOUR_DIMENTION_SHIPS 10
#define LIMIT_THREE_DIMENTION_SHIPS 9
#define LIMIT_TWO_DIMENTION_SHIPS 7
#define LIMIT_ONE_DIMENTION_SHIPS 4

class CShips
{
public:
	enum ShipDirection
	{
		SD_HORIZONTAL,
		SD_VERTICAL
	};
	enum ShipType
	{
		ST_ONE = 1,
		ST_TWO,
		ST_THREE,
		ST_FOUR
	};

	typedef ShipDirection TShipDirection;
	typedef ShipType TShipType;

	CShips();
	~CShips();
	
	void ResetShips();
	void NextShip();
	void ChangeShipDirection();

	int GetShipsCount() const;
	TShipDirection GetShipDirection() const;
	TShipType GetCurrentShipType() const;
	
protected:
private:
	int m_iShipsCount;
	TShipDirection m_eShipDirection;
	TShipType m_eCurrentShipType;
};

#endif /* _SHIPS_H_ */
