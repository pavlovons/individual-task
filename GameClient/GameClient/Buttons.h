#ifndef _MAIN_MENU_BUTTONS_H_
#define _MAIN_MENU_BUTTONS_H_

#include <QtGui/QMainWindow>

class CButtons
{
public:
	CButtons();
	~CButtons();

	bool IsStartButton( QPoint& ) const;
	bool IsLocalGameButton( QPoint& ) const;
	bool IsSettingsButton( QPoint& ) const;	
	bool IsExitButton( QPoint& ) const;
	bool IsConnectButton( QPoint& ) const;
	bool IsBackButton( QPoint& ) const;

	const QRect GetCoordsStartButton() const;
	const QRect GetCoordsLocalGameButton() const;
	const QRect GetCoordsSettingsButton() const;
	const QRect GetCoordsExitButton() const;
	const QRect GetCoordsConnectButton() const;
	const QRect GetCoordsBackButton() const;

	void SetCoordsStartButton( QRect& );
	void SetCoordsLocalGameButton( QRect& );
	void SetCoordsSettingsButton( QRect& );
	void SetCoordsExitButton( QRect& );
	void SetCoordsConnectButton( QRect& );
	void SetCoordsBackButton( QRect& );

private:
	QRect m_RectStartButton;
	QRect m_RectLocalGameButton;
	QRect m_RectSettingsButton;
	QRect m_RectExitButton;
	QRect m_RectConnectButton;
	QRect m_RectBackButton;
};

#endif /* _MAIN_MENU_BUTTONS_H_ */