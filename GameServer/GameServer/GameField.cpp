#include "GameField.h"

namespace sea_battle
{
	CGameField::CGameField(): m_board_length( DEFAULT_FIELD_SIZE ), m_CurrentPlayerTurn( MY ),	m_MyFieldModel( m_board_length, TFieldModelRow( m_board_length ) ), m_EnemyFieldModel( m_board_length, TFieldModelRow( m_board_length ) )
		{
			ClearFields();
		}

		CGameField::~CGameField()
		{
			m_MyFieldModel.clear();
			m_EnemyFieldModel.clear();
		}
		
		void CGameField::SetCurrentPlayer( bool bCurrent )
		{
			if ( bCurrent )
			{
				m_CurrentPlayerTurn = MY;
			}
			else
			{
				m_CurrentPlayerTurn = ENEMY;
			}
		}

		void CGameField::ClearFields()
		{
			for (int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
			{
				TFieldModelRow::iterator itBegin = m_MyFieldModel[i].begin();
				TFieldModelRow::iterator itEnd = m_MyFieldModel[i].end();
				for ( TFieldModelRow::iterator it = itBegin; it != itEnd;  ++it )
				{
					(*it).SetEmpty();
				}
			}
			for (int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
			{
				TFieldModelRow::iterator itBegin = m_EnemyFieldModel[i].begin();
				TFieldModelRow::iterator itEnd = m_EnemyFieldModel[i].end();
				for ( TFieldModelRow::iterator it = itBegin; it != itEnd;  ++it )
				{
					(*it).SetEmpty();
				}
			}
		}

		void CGameField::ChangePlayer()
		{
			if ( IsCurrentPlayer() )
			{
				m_CurrentPlayerTurn = ENEMY;
			}
			else 
			{
				m_CurrentPlayerTurn = MY;
			}
		}
		
		void CGameField::PlaceShipsFromString( string str )
		{
			int k = 0;
			for( int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
			{
				for( int j = 0; j < DEFAULT_FIELD_SIZE; ++j )
				{
					if ( str[k] == '1' )
					{
						m_MyFieldModel[i][j].SetShip();
					}
					k++;
				}
			}
		}	

		void CGameField::MakeMoveMiss( const CFieldPosition& rCurrentPosition )
		{
			m_EnemyFieldModel[rCurrentPosition.GetX()][rCurrentPosition.GetY()].SetDot();
		}
		
		void CGameField::MakeMoveShoot( const CFieldPosition& rCurrentPosition )
		{
			m_EnemyFieldModel[rCurrentPosition.GetX()][rCurrentPosition.GetY()].SetX();
		}

		bool InField( const CFieldPosition& rCurrentPosition )
		{
			return ( rCurrentPosition.GetX() >= 0 && rCurrentPosition.GetY() >= 0 && rCurrentPosition.GetX() < DEFAULT_FIELD_SIZE && rCurrentPosition.GetY() < DEFAULT_FIELD_SIZE );
		}

		bool CGameField::IsShipKilled( const CFieldPosition& rCurrentPosition )
		{
			bool bResult = true;
			CFieldPosition PositionToCheck = rCurrentPosition;
			int x = PositionToCheck.GetX();
			int y = PositionToCheck.GetY();
			do 
			{
				if ( ++x < DEFAULT_FIELD_SIZE)
				{				
					if ( m_MyFieldModel[x][y].IsEmpty() || m_MyFieldModel[x][y].IsDot() )
					{
						break;
					}
					if ( m_MyFieldModel[x][y].IsShip() )
					{	
						bResult = false;
						break;
					}
				}
				else
				{
					break;
				}
			} 
			while ( true );
			x = PositionToCheck.GetX();
			y = PositionToCheck.GetY();
			if( bResult )
			{
			do 
			{
				if ( --x >= 0)
				{
					if ( m_MyFieldModel[x][y].IsEmpty() || m_MyFieldModel[x][y].IsDot() )
					{
						break;
					}
					if ( m_MyFieldModel[x][y].IsShip() )
					{
						bResult = false;
						break;
					}
				}
				else
				{
					break;
				}
			} 
			while ( true );
			}
			x = PositionToCheck.GetX();
			y = PositionToCheck.GetY();
			if( bResult )
			{
			do 
			{
				if ( ++y < DEFAULT_FIELD_SIZE)
				{
					if ( m_MyFieldModel[x][y].IsEmpty() || m_MyFieldModel[x][y].IsDot() )
					{
						break;
					}
					if ( m_MyFieldModel[x][y].IsShip() )
					{
						bResult = false;
						break;
					}
				}
				else
				{
					break;
				}
			} 
			while ( true );
			}
			x = PositionToCheck.GetX();
			y = PositionToCheck.GetY();
			if( bResult )
			{
			do 
			{
				if ( --y >= 0)
				{
					if ( m_MyFieldModel[x][y].IsEmpty() || m_MyFieldModel[x][y].IsDot() )
					{
						break;
					}
					if ( m_MyFieldModel[x][y].IsShip() )
					{
						bResult = false;
						break;
					}
				}
				else
				{
					break;
				}	
			} 
			while ( true );
			}
			return bResult;
		}

		bool CGameField::IsPlayerHasShips()
		{
			bool bResult = false;
			for (int i = 0; i < DEFAULT_FIELD_SIZE; ++i )
			{
				TFieldModelRow::iterator itBegin = m_MyFieldModel[i].begin();
				TFieldModelRow::iterator itEnd = m_MyFieldModel[i].end();
				for ( TFieldModelRow::iterator it = itBegin; it != itEnd;  ++it )
				{
					if ( (*it).IsShip() )
					{
						bResult = true;
						break;
					}
				}
				if ( bResult )
				{
					break;
				}
			}
			return bResult;
		}
		

		int CGameField::TakeTheShoot( const CFieldPosition& rCurrentPosition )
		{
			int iResult = 1;
			const CFieldCell CCurrentCell = m_MyFieldModel[rCurrentPosition.GetX()][rCurrentPosition.GetY()];
			do
			{
				if ( CCurrentCell.IsShooted() )	
				{
					iResult = 0;
					break;
				}
				if ( CCurrentCell.IsEmpty() )
				{
					TakeMoveMiss( rCurrentPosition );
					iResult = -1;
					break;
				}
				else
				{
					TakeMoveShoot( rCurrentPosition );
					if ( IsShipKilled( rCurrentPosition ) )
					{
						iResult = 2;
						break;
					}
					else
					{
						iResult = 1;
						break;
					}
				}
			}
			while ( true );
			return iResult;
		}

		void CGameField::TakeMoveMiss( const CFieldPosition& rCurrentPosition )
		{
			m_MyFieldModel[rCurrentPosition.GetX()][rCurrentPosition.GetY()].SetDot();
		}

		void CGameField::TakeMoveShoot( const CFieldPosition& rCurrentPosition )
		{
			m_MyFieldModel[rCurrentPosition.GetX()][rCurrentPosition.GetY()].SetX();
		}
}