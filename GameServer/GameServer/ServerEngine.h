#ifndef _SERVER_ENGINE_H_
#define _SERVER_ENGINE_H_

#include <QStringList>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMap>
#include <QDomDocument>
#include "GameField.h"

#define BUFFER_SIZE

using namespace sea_battle;

class ServerEngine : public QTcpServer
{
	Q_OBJECT

public:
	ServerEngine(QObject *parent=0);

private slots:
	void readyRead();
	void disconnected();

protected:
	void incomingConnection(int socketfd);

private:
	QTcpSocket* client1;
	QTcpSocket* client2;
	bool bCommandGo;
	CGameField gameFieldsClient1;
	CGameField gameFieldsClient2;
	QMap <QTcpSocket*, QString> logins;
};

#endif
