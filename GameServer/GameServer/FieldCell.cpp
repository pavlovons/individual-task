#include "FieldCell.h"

namespace sea_battle
{
	CFieldCell::CFieldCell()
	{
		m_CellType = EMPTY_CELL;
	}

	CFieldCell::~CFieldCell()
	{

	}

	void CFieldCell::SetDot()
	{
		m_CellType = DOT_CELL;
	}

	void CFieldCell::SetX()
	{
		m_CellType = X_CELL;
	}

	void CFieldCell::SetShip()
	{
		m_CellType = SHIP_CELL;
	}

	void CFieldCell::SetEmpty()
	{
		m_CellType = EMPTY_CELL;
	}

	bool CFieldCell::IsDot() const
	{
		return m_CellType == DOT_CELL;
	}

	bool CFieldCell::IsX() const
	{
		return m_CellType == X_CELL;
	}

	bool CFieldCell::IsShooted() const
	{
		return ( IsX() || IsDot() );
	}

	bool CFieldCell::IsShip() const
	{
		return m_CellType == SHIP_CELL;
	}

	bool CFieldCell::IsEmpty() const
	{
		return m_CellType == EMPTY_CELL;
	}
}