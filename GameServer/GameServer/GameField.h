#ifndef _GAME_FIELD_H_
#define _GAME_FIELD_H_
#include <vector>
#include <string>

#include "FieldCell.h"
#include "FieldPosition.h"

#define DEFAULT_FIELD_SIZE 10

using namespace std;

namespace sea_battle
{
	class CGameField
	{
	public:
		typedef unsigned TPlayer;
		typedef vector< CFieldCell > TFieldModelRow;
		typedef vector< TFieldModelRow > TFieldModel;

		enum PLAYER_TURN
		{
			MY,
			ENEMY
		};

		CGameField();
		~CGameField();

		void ClearFields();
		
		bool IsPlayerHasShips();

		bool InField( const CFieldPosition& );

		bool IsShipKilled( const CFieldPosition& );
		
		bool IsCurrentPlayer() const
		{
			return ( m_CurrentPlayerTurn == MY );
		}

		void SetCurrentPlayer( bool bCurrent );

		void ChangePlayer();
				
		void PlaceShipsFromString( string );

		void MakeMoveMiss(const CFieldPosition&);
		
		void MakeMoveShoot(const CFieldPosition&);

		// 1 - Shoot is successful
		// 0 - Already shooted
		// -1 - Missed
		int TakeTheShoot(const CFieldPosition&);//

		void TakeMoveMiss(const CFieldPosition&);

		void TakeMoveShoot(const CFieldPosition&);
	private:
		unsigned m_board_length;
		PLAYER_TURN m_CurrentPlayerTurn;
		TFieldModel m_MyFieldModel;
		TFieldModel m_EnemyFieldModel;
	};
}

#endif /* _GAME_FIELD_H_ */