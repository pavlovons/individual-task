#include "FieldPosition.h"

namespace sea_battle
{
	CFieldPosition::CFieldPosition()
	{
	
	}
	
	CFieldPosition::~CFieldPosition()
	{
	
	}
	
	CFieldPosition::CFieldPosition(int x, int y)
	{
		m_X = x;
		m_Y = y;
	}
	
	int CFieldPosition::GetX() const
	{
		return m_X;
	}
	
	int CFieldPosition::GetY() const
	{
		return m_Y;
	}
}
