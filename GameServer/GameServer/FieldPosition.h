#ifndef _FIELD_POSITIION_H_
#define _FIELD_POSITIION_H_

namespace sea_battle
{
	class CFieldPosition
	{
	public:
		CFieldPosition();
		~CFieldPosition();

		CFieldPosition(int, int);

		int GetX() const;
		int GetY() const;

	private:
		int m_X;
		int m_Y;
	};
}
#endif /* _FIELD_POSITIION_H_ */