#ifndef _FIELD_CELL_H_
#define _FIELD_CELL_H_

namespace sea_battle
{
	class CFieldCell
	{
	public:
		typedef unsigned TCell;

		enum CELL_TYPE
		{
			X_CELL,
			DOT_CELL,
			EMPTY_CELL,
			SHIP_CELL
		};

		CFieldCell();
		~CFieldCell();

		void SetDot();
		void SetX();
		void SetShooted();
		void SetShip();
		void SetEmpty();	

		bool IsDot() const;
		bool IsX() const;
		bool IsShooted() const;
		bool IsShip() const;
		bool IsEmpty() const;		
	private:
		CELL_TYPE m_CellType;
	};
}

#endif /* _FIELD_CELL_H_ */