#include "ServerEngine.h"

ServerEngine::ServerEngine(QObject *parent) : QTcpServer(parent)
{
	bCommandGo = false;
	client1 = nullptr;
	client2 = nullptr;
}

void ServerEngine::incomingConnection(int socketfd)
{
	if( client1 == nullptr )
	{
		client1 = new QTcpSocket(this);
		client1->setSocketDescriptor(socketfd);
		qDebug() << "Server: New client from:" << client1->peerAddress().toString();
		connect(client1, SIGNAL(readyRead()), this, SLOT(readyRead()));
		connect(client1, SIGNAL(disconnected()), this, SLOT(disconnected()));
	}
	else if( client2 == nullptr )
	{
		client2 = new QTcpSocket(this);
		client2->setSocketDescriptor(socketfd);
		qDebug() << "Server: New client from:" << client2->peerAddress().toString();
		connect(client2, SIGNAL(readyRead()), this, SLOT(readyRead()));
		connect(client2, SIGNAL(disconnected()), this, SLOT(disconnected()));
	}
}

void ServerEngine::readyRead()
{
	QTcpSocket *client = (QTcpSocket*)sender();
	QString line = QString::fromUtf8(client->readLine()).trimmed();
	QDomDocument doc;
	doc.setContent(line);
	QDomElement rootElement= doc.documentElement();
	QString rootName = rootElement.tagName();
	if ( rootName == "seabattle" )
	{	
		if ( rootElement.attribute("command","") == "client" )
		{
			QDomNode domNode = rootElement.firstChild();
			QString login;
			if ( domNode.isElement() && !domNode.isNull())
			{
				QDomElement domElement = domNode.toElement();
				if(!domElement.isNull())
				{
					if(domElement.tagName() == "element") 
					{
						login = domElement.attribute("login", "");
					}
				}
			}
			qDebug() << "Server:" << client->peerAddress().toString() << " logged to server as " << login << ".";
			logins[client] = login;
			QByteArray xmlText =
					"<seabattle command=\"server\">"
					"	<element name=\"SeaBattleServer\"/>"		
					"</seabattle>";
			client->write(QString(xmlText).toUtf8());
		}
		else if ( rootElement.attribute("command","") == "field" )
		{
			QString cells;
			QDomNode domNode = rootElement.firstChild();
			if ( domNode.isElement() && !domNode.isNull())
			{
				QDomElement domElement = domNode.toElement();
				if(!domElement.isNull())
				{
					if(domElement.tagName() == "element") 
					{
						cells = domElement.attribute("cells", "");
					}
				}
			}
			if ( client == client1 )
			{
				gameFieldsClient1.ClearFields();
				gameFieldsClient1.PlaceShipsFromString( cells.toStdString() );
				gameFieldsClient1.SetCurrentPlayer( true );
				if ( bCommandGo )
				{	
					QByteArray xmlText =
						"<seabattle command=\"go\">"
						"	<element name=\" \"/>"		
						"</seabattle>";
					client1->write(QString(xmlText).toUtf8());
				}
				bCommandGo = true;
				qDebug() << "Server: Client " << logins[client1] << " placed his ships.";
			}
			else if ( client == client2 )
			{
				gameFieldsClient2.ClearFields();
				gameFieldsClient2.PlaceShipsFromString( cells.toStdString() );
				gameFieldsClient2.SetCurrentPlayer( false );
				if ( bCommandGo )
				{	
					QByteArray xmlText =
						"<seabattle command=\"go\">"
						"	<element name=\" \"/>"		
						"</seabattle>";
					client1->write(QString(xmlText).toUtf8());					
				}
				bCommandGo = true;
				qDebug() << "Server: Client " << logins[client1] << " placed his ships.";
			}
		}
		else if ( rootElement.attribute("command","") == "step" )
		{
			QDomNode domNode = rootElement.firstChild();
			int x, y;
			if ( domNode.isElement() && !domNode.isNull())
			{
				QDomElement domElement = domNode.toElement();
				if(!domElement.isNull())
				{
					if(domElement.tagName() == "element") 
					{
						x = domElement.attribute("x", "").toInt();							
					}
				}
			}
			domNode = domNode.nextSibling();
			if ( domNode.isElement() && !domNode.isNull())
			{
				QDomElement domElement = domNode.toElement();
				if(!domElement.isNull())
				{
					if(domElement.tagName() == "element") 
					{
						y = domElement.attribute("y", "").toInt();		
					}
				}
			}
			if ( client == client1 )
			{
				qDebug() << "Client: " << logins[client1] << ": Make move to (" << x + 1 << ", " << y + 1 << ").";
				CFieldPosition ShootingPosition = CFieldPosition(x, y);
				int iShootType = gameFieldsClient2.TakeTheShoot( ShootingPosition );
				char *buffer = new char[BUFFER_SIZE];
				QByteArray xmlText;
				switch(iShootType)
				{
				case -1:
					gameFieldsClient1.MakeMoveMiss( ShootingPosition );
					sprintf( buffer,
						"<seabattle command=\"miss\">"
						"	<element x=\"%d\"/>"
						"	<element y=\"%d\"/>"
						"</seabattle>", x, y);
					client1->write(QString(buffer).toUtf8());
					client2->write(QString(buffer).toUtf8());
					gameFieldsClient1.ChangePlayer();
					gameFieldsClient2.ChangePlayer();
					qDebug() << "Client: " << logins[client1] << ": Missed at (" << x + 1 << ", " << y + 1 << ").";
					xmlText =
						"<seabattle command=\"go\">"
						"	<element name=\" \"/>"		
						"</seabattle>";
					client2->write(QString(xmlText).toUtf8());
						break;
				case 1:
					gameFieldsClient1.MakeMoveShoot( ShootingPosition );
					qDebug() << "Client: " << logins[client1] << ": Halt shotted at (" << x + 1 << ", " << y + 1 << ").";
					sprintf( buffer,
						"<seabattle command=\"half\">"
						"	<element x=\"%d\"/>"
						"	<element y=\"%d\"/>"
						"</seabattle>", x, y);
					client1->write(QString(buffer).toUtf8());
					client2->write(QString(buffer).toUtf8());
					break;
				case 2:
					gameFieldsClient1.MakeMoveShoot( ShootingPosition );
					qDebug() << "Client: " << logins[client1] << ": Killed at (" << x + 1 << ", " << y + 1 << ").";
					if ( !gameFieldsClient2.IsPlayerHasShips() )
					{
						qDebug() << "Client: " << logins[client1] << ": Win the game.";
						buffer = 
							"<seabattle command=\"win\">"
							"	<element name=\"\"/>"
							"</seabattle>";
						client1->write(QString(buffer).toUtf8());
						qDebug() << "Client: " << logins[client2] << ": Lose the game.";
						buffer = 
							"<seabattle command=\"lose\">"
							"	<element name=\"\"/>"
							"</seabattle>";
						client2->write(QString(buffer).toUtf8());
						break;
					}	
					sprintf( buffer,
						"<seabattle command=\"kill\">"
						"	<element x=\"%d\"/>"
						"	<element y=\"%d\"/>"
						"</seabattle>", x, y);
					client1->write(QString(buffer).toUtf8());
					client2->write(QString(buffer).toUtf8());
					break;
				default:
						break;
				}
			}
			else if ( client == client2 )
			{
				qDebug() << "Client: " << logins[client2] << ": Make move to (" << x + 1 << ", " << y + 1 << ").";
				CFieldPosition ShootingPosition = CFieldPosition(x, y);
				int iShootType = gameFieldsClient1.TakeTheShoot( ShootingPosition );
				char *buffer = new char[BUFFER_SIZE];
				QByteArray xmlText;
				switch(iShootType)
				{
				case -1:
					gameFieldsClient2.MakeMoveMiss( ShootingPosition );
					sprintf( buffer,
						"<seabattle command=\"miss\">"
						"	<element x=\"%d\"/>"
						"	<element y=\"%d\"/>"
						"</seabattle>", x, y);
					client1->write(QString(buffer).toUtf8());
					client2->write(QString(buffer).toUtf8());
					gameFieldsClient1.ChangePlayer();
					gameFieldsClient2.ChangePlayer();
					qDebug() << "Client: " << logins[client2] << ": Missed at (" << x + 1 << ", " << y + 1 << ").";
					xmlText =
						"<seabattle command=\"go\">"
						"	<element name=\" \"/>"		
						"</seabattle>";
					client1->write(QString(buffer).toUtf8());
					break;
				case 1:
					gameFieldsClient2.MakeMoveShoot( ShootingPosition );
					qDebug() << "Client: " << logins[client2] << ": Halt shotted at (" << x + 1 << ", " << y + 1 << ").";
					sprintf( buffer,
						"<seabattle command=\"half\">"
						"	<element x=\"%d\"/>"
						"	<element y=\"%d\"/>"
							"</seabattle>", x, y);
					client1->write(QString(buffer).toUtf8());
					client2->write(QString(buffer).toUtf8());
					break;
				case 2:
					gameFieldsClient2.MakeMoveShoot( ShootingPosition );
					qDebug() << "Client: " << logins[client2] << ": Killed at (" << x + 1 << ", " << y + 1 << ").";
					if ( !gameFieldsClient1.IsPlayerHasShips() )
					{
						qDebug() << "Client: " << logins[client2] << ": Win the game.";
						buffer = 
							"<seabattle command=\"lose\">"
							"	<element name=\"\"/>"
							"</seabattle>";
						client1->write(QString(buffer).toUtf8());
						qDebug() << "Client: " << logins[client2] << ": Lose the game.";
						buffer = 
							"<seabattle command=\"win\">"
							"	<element name=\"\"/>"
							"</seabattle>";
						client2->write(QString(buffer).toUtf8());
						break;
					}						
					sprintf( buffer,
						"<seabattle command=\"kill\">"
						"	<element x=\"%d\"/>"
						"	<element y=\"%d\"/>"
						"</seabattle>", x, y);
					client1->write(QString(buffer).toUtf8());
					client2->write(QString(buffer).toUtf8());
						break;
				default:
						break;
				}
			}
		}	
	}
	else
	{
		qWarning() << "Got bad message from client:" << client->peerAddress().toString() << line;
	}
}

void ServerEngine::disconnected()
{
	QTcpSocket *client = (QTcpSocket*)sender();
	qDebug() << "Server: Client disconnected: " << logins[client];
	logins.remove(client);
	if ( client == client1 )
	{
		client2->disconnect();
	}
	else if ( client == client2 )
	{
		client1->disconnect();
	}
}
